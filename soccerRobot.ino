#include <Servo.h>
//#include <SoftwareSerial.h>

//SoftwareSerial mySerial(3, 4); // RX, TX

String rec = "";
String left;
String right;
String temp;
bool calibrate = false;

unsigned long timer = millis();

int currentAngle = 0;
int desiredAngle = 0;

const int THRESHOLD = 20;

Servo servo1;
Servo servo2;

int speedL = 0;
int speedR = 0;

int speedLPrev = 0;
int speedRPrev = 0;

unsigned long long kickTimer = 0;


void setup() {

  pinMode(13, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(5,OUTPUT);

  //servo1.attach(3);
  //servo2.attach(4);//3 and 4
  //mySerial.begin(115200);
  //mySerial.println("Hello world!");
  Serial.begin(115200);
  delay(5000);

  digitalWrite(2, 1);

  delay(1000);  

  // Serial.write("AT+CIPMUX=1\r\n");
  // delay(1000);
  
  // Serial.write("AT+CIPSERVER=1,1336\r\n");
  delay(10000);

  Serial.write("AT+CIPSTART=\"UDP\",\"0.0.0.0\",1337,1338,2\r\n");
  delay(1000);

  digitalWrite(13, 1);
/*
unsigned long timer = millis();

  while (millis()-timer<10000){
  if (mySerial.available())
    Serial.write(mySerial.read());
  if (Serial.available())
    mySerial.write(Serial.read());
  }
  mySerial.println("Time's up!!\n");
  delay(1000);
  */
}

void loop() {
  // put your main code here, to run repeatedly:
// if (Serial1.available()) Serial.write(Serial1.read());

  // Keep reading from Arduino Serial Monitor and send to HC-05
 // if (Serial.available()) Serial1.write(Serial.read());

  rec=Serial.readStringUntil('\n');
  if(rec.startsWith("+IPD,")){
    rec = rec.substring(rec.indexOf(':')+1);
    rec.remove(rec.length()-1);

    if(rec.startsWith("calibrate")){
      calibrate = true;
    }
    
    if(rec.startsWith("curAng")){
      currentAngle = rec.substring(rec.indexOf(' ')+1).toInt();
      
    }else if(rec.startsWith("desAng")){
      desiredAngle = rec.substring(rec.indexOf(' ')+1).toInt();
      
    }else if(rec.startsWith("speedL ")){
      speedL = rec.substring(rec.indexOf(' ')+1).toInt();
    }else if(rec.startsWith("speedR ")){
      speedR = rec.substring(rec.indexOf(' ')+1).toInt();
    }else if(rec.startsWith("speedLR ")){
      temp = rec.substring(rec.indexOf(' ')+1);
      int j1 = 0;
      left = "";
      right = "";
      while(temp[j1]!=' '){
        left+= temp[j1];
        j1++;
      }
      j1++;
      while(temp[j1]!=' '){
        right+= temp[j1];
        j1++;
      }
      speedR = right.toInt();
      speedL = left.toInt();
      timer = millis();
      
    }else if(rec.startsWith("kick")){
      //kickTimer = millis();
      digitalWrite(5,1);
      delay(50);
      digitalWrite(5,0);
    }

    /*if (millis() - timer > 500){
      speedR = 0;
      speedL = 0;
    }*/

    /*
    if(rec=="l"){
      servo1.write(0);
    }
    else if(rec=="r"){
      servo1.write(180);
    }
    else if(rec=="s"){
      servo1.write(90);
    }*/
  }

  /*if(millis() - kickTimer>100){
    digitalWrite(5,0);
  }*/

/*
  if(abs(currentAngle-desiredAngle)<THRESHOLD){
    servo1.write(90);
    servo2.write(90);
  }else if(currentAngle > (desiredAngle + THRESHOLD)){
    servo1.write(95);
    servo2.write(95);
  }else if(currentAngle < (desiredAngle - THRESHOLD)){
    servo1.write(85);
    servo2.write(85);
  }
*/
  if(speedLPrev!=speedL||speedRPrev!=speedR){
    if (speedL==0 && !calibrate){
      if(servo1.attached()){
        servo1.detach();
      }
    }else{
      if(!servo1.attached()){
        servo1.attach(3);
      }
      servo1.write(((speedL+100)*9)/10);
    }

    if (speedR==0 && !calibrate){
      if(servo2.attached()){
        servo2.detach();
      }
    }else{
      if(!servo2.attached()){
        servo2.attach(4);
      }
      servo2.write(((speedR+100)*9)/10);
    }
    
    speedLPrev=speedL;
    speedRPrev=speedR;
  }
  
  
}
